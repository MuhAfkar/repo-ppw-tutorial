from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Muhammad Afkar'
# TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = datetime.now()
    return (((today.year - 1998) * 365) + ((today.month - 10) * 30) + today.day - 28) / 365

